package com.example.demo.controller;

import com.example.demo.model.AjiaUser;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class SampleController {

    @GetMapping("/index")
    public String doNothing(@AuthenticationPrincipal AjiaUser user, Model model) {
        model.addAttribute("page", "LandingPage");

        return "index";
    }

    @GetMapping("/index/ref={reference}")
    public String getReference(@PathVariable("reference") String reference, Model model) {
        var user = new AjiaUser("name", "name", List.of());
        user.setBearerToken(reference.toCharArray());

        var authDetails = new UsernamePasswordAuthenticationToken(
                user,
                null,
                user.getAuthorities());

        SecurityContextHolder.getContext().setAuthentication(authDetails);

        model.addAttribute("page", reference);

        return "index";
    }

    @GetMapping("/fire")
    public String fireAway(@AuthenticationPrincipal AjiaUser user, Model model) {
        String token = String.valueOf(user.getBearerToken());
        model.addAttribute("page", token);

        System.out.println("We can do anything we want with token " + token);
        return "fire";
    }
}
